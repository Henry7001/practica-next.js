export interface User {
    id: number;
    nombre: string;
    apellido: string;
    email: string;
    cedula: string;
    telefono: string;
    idRol: string;
}