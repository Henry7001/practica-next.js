"use client";
import { User } from "@/interfaces/User.interface";
import React, { use, useState } from "react";
import { css } from "../../../styled-system/css";
import { gql, useMutation, useQuery } from "@apollo/client";

const CrearUsuario: React.FC = () => {
  const create = gql`
    mutation CreateUser(
      $nombre: String!
      $apellido: String!
      $email: String!
      $cedula: String!
      $telefono: String!
      $idRol: String!
    ) {
      createUser(
        nombre: $nombre
        apellido: $apellido
        email: $email
        cedula: $cedula
        telefono: $telefono
        idRol: $idRol
      ) {
        id
      }
    }
  `;

  const [user, setUser] = useState<User>({
    id: 0,
    nombre: "",
    apellido: "",
    email: "",
    cedula: "",
    telefono: "",
    idRol: "2",
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUser((prevUser) => ({
      ...prevUser,
      [e.target.name]: e.target.value,
    }));
  };

const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    createUser({ variables: { ...user } });
    window.location.href = "/";
};

  const [createUser, { loading, error, data }] = useMutation(create, {
    variables: {
        nombre: user.nombre,
        apellido: user.apellido,
        email: user.email,
        cedula: user.cedula,
        telefono: user.telefono,
        idRol: user.idRol,
        },
  });
  
  

  const Form = css({
    display: "flex",
    flexDirection: "column",
    width: "100%",
    maxWidth: "500px",
    margin: "0 auto",
    padding: "20px",
    border: "1px solid #ccc",
    borderRadius: "4px",
    "& h1": {
      textAlign: "center",
    },
    "& label": {
      marginBottom: "5px",
    },
    "& input": {
      width: "100%",
      padding: "10px",
      marginBottom: "10px",
      boxSizing: "border-box",
      borderRadius: "4px",
      border: "1px solid #ccc",
    },
    "& select": {
      width: "100%",
      padding: "10px",
      marginBottom: "10px",
      boxSizing: "border-box",
      borderRadius: "4px",
      border: "1px solid #ccc",
    },
    "& button": {
      padding: "10px",
      borderRadius: "4px",
      border: "none",
      backgroundColor: "#333",
      color: "#fff",
      cursor: "pointer",
    },
  });

  const padding = css({
    padding: "5",
  });

  const titulo = css({
    textAlign: "center",
    fontSize: "4rem",
    marginBottom: "20px",
  });

  return (
    <div className={padding}>
      <h1 className={titulo}>Crear usuario</h1>
      <form className={Form} onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Nombre:</label>
          <input
            type="text"
            id="nombre"
            name="nombre"
            value={user.nombre}
            onChange={handleChange}
          />
        </div>

        <div>
          <label htmlFor="apellido">Apellido:</label>
          <input
            type="text"
            id="apellido"
            name="apellido"
            value={user.apellido}
            onChange={handleChange}
          />
        </div>

        <div>
          <label htmlFor="cedula">Cedula:</label>
          <input
            type="text"
            id="cedula"
            name="cedula"
            value={user.cedula}
            onChange={handleChange}
          />
        </div>

        <div>
          <label htmlFor="telefono">Telefono:</label>
          <input
            type="text"
            id="telefono"
            name="telefono"
            value={user.telefono}
            onChange={handleChange}
          />
        </div>

        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            value={user.email}
            onChange={handleChange}
          />
        </div>

        <button type="submit">Guardar</button>
      </form>
    </div>
  );
};

export default CrearUsuario;