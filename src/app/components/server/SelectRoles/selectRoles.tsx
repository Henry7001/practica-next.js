'use server';
import { gql, useQuery } from "@apollo/client";

interface SelectRolesProps {
  user: any;
  handleSelectChange: any;
}

const selectRoles: React.FC<SelectRolesProps> = ({user}) => {
    const query = gql`
        query Query {
            Roles {
                id
                nombre
            }
        }
    `;
    const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        user.idRol = e.target.value;
    };

    const { loading, error, data } = useQuery(query);
    return (
        <>
            {loading && <p>Loading...</p>}
            {error && <p>Error: {error.message}</p>}
            {data &&
                !loading &&
                !error && (
                    <>
                        <select
                            id="rol"
                            name="idRol"
                            value={user.idRol}
                            onChange={handleSelectChange}
                        >
                            <option value="">Seleccionar</option>
                            {data.Roles.map((rol:any) => (
                                <option key={rol.id} value={rol.id}>
                                    {rol.nombre}
                                </option>
                            ))}
                        </select>
                    </>
                )}
        </>
    )
}
export default selectRoles;