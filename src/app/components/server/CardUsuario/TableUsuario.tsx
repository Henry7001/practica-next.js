"use client";
import { useQuery, gql } from "@apollo/client";
import { css } from "../../../../../styled-system/css";

function TableUsuario() {
  const { data, loading, error } = useQuery(gql`
    query Usuarios {
      Usuarios {
        id
        nombre
        apellido
        email
        cedula
        telefono
        rol {
          nombre
        }
      }
    }
  `);

  const grid = css({
    borderStyle: "solid",
    borderBlockColor: "black",
    borderWidth: 1,
    padding: "1.5",
  });

  const tablaCentrada = css({
    display: "flex",
    justifyContent: "center",
  });

  const Tabla = css({
    borderCollapse: "collapse",
    width: "100%",
    "& th": {
      border: "1px solid #ddd",
      padding: "8px",
    },
    "& td": {
      border: "1px solid #ddd",
      padding: "8px",
    },
    "& tr:nth-child(even)": {
      backgroundColor: "#f2f2f2",
    },
    "& tr:hover": {
      backgroundColor: "#f1f1f1",
    },
  });

  const Button = css({
    backgroundColor: "#4CAF50",
    border: "none",
    color: "white",
    padding: "15px 32px",
    textAlign: "center",
    textDecoration: "none",
    display: "inline-block",
    fontSize: "16px",
    margin: "4px 2px",
    cursor: "pointer",
  });
  

  return (
    <div className={css({ padding: "5" })}>
      {loading && <p>Loading...</p>}
      {error && <p>Error: {error.message}</p>}
      {data &&
        !loading &&
        !error && (
          <>
          <a className={Button} href="/CrearUsuario">Crear Usuario</a>
          <table className={Tabla}>
            <thead>
              <tr>
                <th>Nombres</th>
                <th>Correo</th>
                <th>Cédula</th>
                <th>Teléfono</th>
                <th>Rol</th>
              </tr>
            </thead>
            <tbody>
              {data.Usuarios.map((usuario: any) => (
                <tr>
                  <td>
                    {usuario.nombre} {usuario.apellido}
                  </td>
                  <td>{usuario.email}</td>
                  <td>{usuario.cedula}</td>
                  <td>{usuario.telefono}</td>
                  <td>{usuario.rol.nombre}</td>
                </tr>
              ))}
            </tbody>
          </table>
          </>
        )}
    </div>
  );
}

export default TableUsuario;
